import pandas as pd
import psycopg2 as pg
import subprocess as sp
import matplotlib.pyplot as plt



# nom = input("Nom d'utilisateur : ")
# print(nom)
# util = 'db' + nom
# print(util)
sp.run(["stty", "-echo"])


mdp = input("Le mot de passe stp : ")


sp.run(["stty", "echo"])

co = None
try:
    # Connexion à la base
    # Attention ! pensez à remplacer dblogin , login et mot_de_passe
    #avec vos informations
    co = pg.connect(host='londres',
                    database ='dblolaborie',
                    user='lolaborie',
                    password = mdp)
    
    df = pd.read_sql('''SELECT p.nomR,avg(s.position) as Moyenne_Position_Region
                        FROM Statistique s, Pays p, Region r
                        WHERE s.codeP = p.code AND p.nomR = r.nom AND s.annee = '2022'
                        GROUP BY p.nomR;''', con=co) # Position moyenne par région en 2022
    plt.plot(df['nomr'], df['moyenne_position_region'],'g')
    plt.plot(df['nomr'], df['moyenne_position_region'],'xk')
    plt.title("Position moyenne par région en 2022")
    plt.show() # Affichage

    df = pd.read_sql('''SELECT p.nomR,avg(s.position) as Moyenne_Position_Region
                        FROM Statistique s, Pays p, Region r
                        WHERE s.codeP = p.code AND p.nomR = r.nom AND s.annee = '2020'
                        GROUP BY p.nomR;''', con=co) # Position moyenne par région en 2020
    plt.plot(df['nomr'], df['moyenne_position_region'],'y')
    plt.plot(df['nomr'], df['moyenne_position_region'],'xk')
    plt.title("Position moyenne par région en 2020")
    plt.show() # Affichage


    df = pd.read_sql('''SELECT p.nom,s.jkill as journalistes_tues
                        FROM Statistique s, Pays p
                        WHERE s.codeP = p.code AND s.annee = '2022'
                        ORDER BY s.jkill DESC
                        LIMIT 5;''', con=co) # les 5 pays où le nombre de journalistes tués en 2022 est le plus élevé
    plt.plot(df['nom'], df['journalistes_tues'],'r')
    plt.plot(df['nom'], df['journalistes_tues'],'xb')
    plt.title("les 5 pays où le nombre de journalistes tués en 2022 est le plus élevé")
    plt.show() # Affichage


    df = pd.read_sql('''SELECT p.nom,s.jkill as journalistes_tues
                        FROM Statistique s, Pays p
                        WHERE s.codeP = p.code AND s.annee = '2020'
                        ORDER BY s.jkill DESC
                        LIMIT 5;''', con=co) # les 5 pays où le nombre de journalistes tués en 2020 est le plus élevé
    plt.plot(df['nom'], df['journalistes_tues'],'r')
    plt.plot(df['nom'], df['journalistes_tues'],'xk')
    plt.title("les 5 pays où le nombre de journalistes tués en 2020 est le plus élevé")
    plt.show() # Affichage

    df = pd.read_sql('''SELECT p.nom,s.mkill as personnes_tues
                        FROM Statistique s, Pays p
                        WHERE s.codeP = p.code AND s.annee = '2022'
                        ORDER BY s.mkill DESC
                        LIMIT 5;''', con=co) # les 5 pays où le nombre de personnes travaillant dans les médias tuées en 2022 est le plus élevé
    plt.plot(df['nom'], df['personnes_tues'],'b')
    plt.plot(df['nom'], df['personnes_tues'],'xr')
    plt.title("les 5 pays où le nombre de personnes travaillant dans les médias tuées en 2022 est le plus élevé")
    plt.show() # Affichage

    df = pd.read_sql('''SELECT p.nom,s.mkill as personnes_tues
                        FROM Statistique s, Pays p
                        WHERE s.codeP = p.code AND s.annee = '2020'
                        ORDER BY s.mkill DESC
                        LIMIT 5;''', con=co) # les 5 pays où le nombre de personnes travaillant dans les médias tuées en 2020 est le plus élevé
    plt.plot(df['nom'], df['personnes_tues'],'b')
    plt.plot(df['nom'], df['personnes_tues'],'xg')
    plt.title("les 5 pays où le nombre de personnes travaillant dans les médias tuées en 2020 est le plus élevé")
    plt.show() # Affichage


# Affichage du message d'erreur en cas de probl ème de connexion
except(Exception, pg.DatabaseError) as error :
    print(error)
# Attention ! Toujours fermer la connexion lorsqu 'on en a plus besoin
finally:
    if co is not None:
        co.close()